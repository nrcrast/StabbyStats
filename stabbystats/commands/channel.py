import discord 
import sqlite3
from stabbystats.commands import StabbyCommand, GraphData
from stabbystats.stabby_database.messages import Messages
from stabbystats.stabby_database.util import get_date_lookback
from stabbystats.stabby_database.util import back_unit_to_readable
import io
import unicodedata
import argparse
import shlex

__all__ = ["ChannelCommand"]


class ChannelCommand(StabbyCommand):
    command = "channel"
    description = "Get stats on a specific channel"

    def __init__(self):
        self.parser = argparse.ArgumentParser(prog='!stabbystats channel', add_help=False, description=self.description, usage=None)
        self.parser.add_argument('--channel','-c', nargs='?', help='Channel Name or ID')
        self.parser.add_argument('--time', '-t', nargs='?', help='Time period(daily, weekly, monthly, yearly, forever)', default='monthly')
        self.parser.add_argument('--private','-p', action='store_true')
        self.usage = self.parser.format_help()

    def parse_args(self, args, message):
        split_args = shlex.split(args)
        try:
            arg_obj = self.parser.parse_args(split_args)

        except SystemExit:
            return None

        if arg_obj.channel != 'all' and arg_obj.channel != 'overall':
            if arg_obj.channel == None:
                arg_obj.channel = message.channel.id
            else:
                arg_obj.channel = self.str_to_channel(arg_obj.channel, message.server)

                if arg_obj.channel == None:
                    arg_obj.channel = message.channel.id
                else:
                    arg_obj.channel = arg_obj.channel.id

            arg_obj.time = get_date_lookback(arg_obj.time)

        return arg_obj

    async def handle(self, client, message, args):
        msgs = Messages()

        arg_obj = self.parse_args(args, message)

        if not arg_obj:
            await client.send_message(message.channel, 'Sorry, I couldn\'t understand your arguments.\n```' + self.usage + '```')
            return
            
        if arg_obj.channel == 'overall' or arg_obj.channel == 'all':
            channel_name = arg_obj.channel
        else:
            channel_name = message.server.get_channel(arg_obj.channel).name

        data = msgs.get_msgs_channel_over_time(message.server.id, arg_obj.channel, arg_obj.time)

        msgs_hr = GraphData('Msgs/Hr For Channel {}({})'.format(unicodedata.normalize('NFKD', channel_name), back_unit_to_readable(arg_obj.time)),
            'Time', 'Msgs/Hr')

        msgs_hr.x_data = data['data']['hour'].x
        msgs_hr.y_data = data['data']['hour'].y

        msgs_day = GraphData('Msgs/Day For Channel {}({})'.format(unicodedata.normalize('NFKD', channel_name), back_unit_to_readable(arg_obj.time)),
            'Time', 'Msgs/Day')

        msgs_day.x_data = data['data']['day'].x
        msgs_day.y_data = data['data']['day'].y

        msgs_wk = GraphData('Msgs/Week For Channel {}({})'.format(unicodedata.normalize('NFKD', channel_name), back_unit_to_readable(arg_obj.time)),
            'Time', 'Msgs/Week')

        msgs_wk.x_data = data['data']['week'].x
        msgs_wk.y_data = data['data']['week'].y

        if data['TotalMsgs'] > 0:
            msg_content = '```Total Msgs: {}\nTotal Words: {}\nAvg Words/Msg: {:.2f}\n```'.format(
                data['TotalMsgs'], data['TotalWords'], data['TotalWords'] / data['TotalMsgs'])
            if arg_obj.private:
                await client.send_file(message.author, fp = io.BytesIO(self.plot([msgs_hr, msgs_day, msgs_wk])), filename='graph.png', content = msg_content)
            else:
                await client.send_file(message.channel, fp = io.BytesIO(self.plot([msgs_hr, msgs_day, msgs_wk])), filename='graph.png', content = msg_content)
