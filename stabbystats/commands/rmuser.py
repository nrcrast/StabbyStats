import discord 
import sqlite3
from stabbystats.commands import StabbyCommand, GraphData
from stabbystats.stabby_database.permissions import Permissions
from stabbystats.stabby_database.util import get_date_lookback
from stabbystats.stabby_database.util import back_unit_to_readable
import unicodedata
import argparse
import shlex
__all__ = ["RmUserCommand"]


class RmUserCommand(StabbyCommand):
    admin_only = True
    command = "rmuser"
    description = "Remove user from permissions"

    def __init__(self):
        self.parser = argparse.ArgumentParser(prog='!stabbystats rmuser', add_help=False, description=self.description, usage=None)
        self.parser.add_argument('--user','-u', nargs='?', help='Username or User ID', required = True)
        self.usage = self.parser.format_help()

    def parse_args(self, args, message):
        split_args = shlex.split(args)
        try:
            arg_obj = self.parser.parse_args(split_args)

        except SystemExit:
            return None

        if arg_obj.user:
            arg_obj.user = self.str_to_member(arg_obj.user, message.server)

        return arg_obj

    async def handle(self, client, message, args):
        perms = Permissions()
        
        arg_obj = self.parse_args(args, message)

        if not arg_obj:
            await client.send_message(message.channel, 'Sorry, I couldn\'t understand your arguments.\n```' + self.usage + '```')
            return

        if arg_obj.user == None:
            await client.send_message(message.channel, 'Sorry, cannot find user: {}'.format(args))
        else:
            perms.rm_user(message.server.id, arg_obj.user.id)
            await client.send_message(message.channel, 'Removed User: {}'.format(arg_obj.user.name))