import discord
from stabbystats.commands import StabbyCommand
import subprocess


__all__ = ["InfoCommand"]


class InfoCommand(StabbyCommand):
    command = "info"
    description = "Get bot info."

    async def handle(self, client, message, args):
        em = discord.Embed(title="StabbyStats Info", colour=0x00DD00)
        em.add_field(name='Git URL', value='https://gitlab.com/nrcrast/StabbyStats', inline = False)
        em.add_field(name='Web Page', value='https://nrcrast.gitlab.io/StabbyStats', inline = False)
        em.add_field(name='Author', value='SuperStabby', inline = False)
        em.add_field(name='Support', value='https://discord.gg/GEhvd6j', inline = False)
        em.add_field(name='Invites', value='[Click here to invite me to your guild!](https://discordapp.com/oauth2/authorize?permissions=125952&scope=bot&client_id=284164350074421248)', inline = False)
        em.add_field(name='Num Servers', value=str(len(client.servers)), inline = False)
        git_commit = subprocess.check_output(["git", "log", "-1", "--oneline"])
        em.add_field(name='Last Commit', value=git_commit.decode('utf-8'), inline = False)

        await client.send_message(message.channel, embed=em)