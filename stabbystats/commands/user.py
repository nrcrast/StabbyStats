import discord 
import sqlite3
from stabbystats.commands import StabbyCommand, GraphData
from stabbystats.stabby_database.messages import Messages
from stabbystats.stabby_database.util import get_date_lookback
from stabbystats.stabby_database.util import back_unit_to_readable
import io
import unicodedata
import argparse
import shlex

__all__ = ["UserCommand"]


class UserCommand(StabbyCommand):
    command = "user"
    description = "Get stats on a single user(or yourself). If private modifier is used, data is sent via DM."
    examples = ["!stabbystats user superstabby monthly", "!stabbystats user forever"]

    def __init__(self):
        self.parser = argparse.ArgumentParser(prog='!stabbystats user', add_help=False, description=self.description, usage=None)
        self.parser.add_argument('--user','-u', nargs='?', help='Username or User ID')
        self.parser.add_argument('--time', '-t', nargs='?', help='Time period(daily, weekly, monthly, yearly, forever)', default='monthly')
        self.parser.add_argument('--private','-p', action='store_true')
        self.usage = self.parser.format_help()

    def parse_args(self, args, message):
        split_args = shlex.split(args)
        try:
            arg_obj = self.parser.parse_args(split_args)

        except SystemExit:
            return None

        if arg_obj.user == None:
            arg_obj.user = message.author
        else:
            arg_obj.user = self.str_to_member(arg_obj.user, message.server)

            if arg_obj.user == None:
                arg_obj.user = message.author

        arg_obj.time = get_date_lookback(arg_obj.time)

        return arg_obj

    def get_usage(self):
        return self.parser.format_usage()

    async def handle(self, client, message, args):
        msgs = Messages()

        arg_obj = self.parse_args(args, message)

        if not arg_obj:
            await client.send_message(message.channel, 'Sorry, I couldn\'t understand your arguments.\n```' + self.usage + '```')
            return

        if arg_obj.private:
            dest = message.author
        else:
            dest = message.channel

        words_hr_data = msgs.get_user_data(message.server.id, arg_obj.user.id, arg_obj.time)

        words_hr = GraphData('Words/Hr For User {}({})'.format(unicodedata.normalize('NFKD', arg_obj.user.name), back_unit_to_readable(arg_obj.time)),
            'Time', 'Words/Hr')

        words_hr.x_data = words_hr_data['x_data']
        words_hr.y_data = words_hr_data['y_data']

        words_day_data = msgs.get_user_data(message.server.id, arg_obj.user.id, arg_obj.time, granularity_hours = 24)

        words_day = GraphData('Words/Day For User {}({})'.format(unicodedata.normalize('NFKD', arg_obj.user.name), back_unit_to_readable(arg_obj.time)),
            'Time', 'Words/Day')

        words_day.x_data = words_day_data['x_data']
        words_day.y_data = words_day_data['y_data']

        words_wk_data = msgs.get_user_data(message.server.id, arg_obj.user.id, arg_obj.time, granularity_hours = 168)

        words_wk = GraphData('Words/Week For User {}({})'.format(unicodedata.normalize('NFKD', arg_obj.user.name), back_unit_to_readable(arg_obj.time)),
            'Time', 'Words/Week')

        words_wk.x_data = words_wk_data['x_data']
        words_wk.y_data = words_wk_data['y_data']

        if words_hr_data['TotalMsgs'] > 0:
            msg_content = '```Total Msgs: {}\nTotal Words: {}\nAvg Words/Msg: {:.2f}\nMax Words/Msg: {}\n```'.format(
                words_hr_data['TotalMsgs'], words_hr_data['TotalWords'], words_hr_data['TotalWords'] / words_hr_data['TotalMsgs'], words_hr_data['Max'])

            await client.send_file(dest, fp = io.BytesIO(self.plot([words_hr, words_day, words_wk])), filename='graph.png', content = msg_content)

