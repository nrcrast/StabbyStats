import discord 
import sqlite3
from stabbystats.commands import StabbyCommand, GraphData
from stabbystats.stabby_database.permissions import Permissions
from stabbystats.stabby_database.util import get_date_lookback
from stabbystats.stabby_database.util import back_unit_to_readable
import unicodedata
__all__ = ["PermDumpCommand"]


class PermDumpCommand(StabbyCommand):
    admin_only = True
    command = "permdump"
    description = "Dump permissions for server"
    usage = "!stabbystats permdump"

    async def handle(self, client, message, args):
        perms = Permissions()
        perm_data = perms.dumps(message.server)
        await client.send_message(message.channel, perm_data)