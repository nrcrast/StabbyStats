import discord 
import sqlite3
from stabbystats.commands import StabbyCommand, GraphData
from stabbystats.stabby_database.messages import Messages
from stabbystats.stabby_database.util import get_date_lookback
from stabbystats.stabby_database.util import back_unit_to_readable
import unicodedata
import argparse
import shlex

__all__ = ["RankCommand"]

class RankCommand(StabbyCommand):
    command = "rank"
    description = "Rank channels"

    def __init__(self):
        self.parser = argparse.ArgumentParser(prog='!stabbystats rank', add_help=False, description=self.description, usage=None)
        self.parser.add_argument('--time', '-t', nargs='?', help='Time period(daily, weekly, monthly, yearly, forever)', default='daily')
        self.parser.add_argument('--private','-p', action='store_true')
        self.parser.add_argument('--all','-a', action='store_true')
        self.usage = self.parser.format_help()

    def parse_args(self, args):
        split_args = shlex.split(args)
        try:
            arg_obj = self.parser.parse_args(split_args)

        except SystemExit:
            return None

        arg_obj.time = get_date_lookback(arg_obj.time)
        return arg_obj

    async def handle(self, client, message, args):
        msgs = Messages()

        arg_obj = self.parse_args(args)

        if not arg_obj:
            await client.send_message(message.channel, 'Sorry, I couldn\'t understand your arguments.\n```' + self.usage + '```')
            return

        if arg_obj.all:
            rank = msgs.get_rank(message.server.id, arg_obj.time, limit = None)
        else:
            rank = msgs.get_rank(message.server.id, arg_obj.time)

        title = 'Channel Rank({})'.format(back_unit_to_readable(arg_obj.time))

        msg_data = '```markdown\n{}\n{}\n\n'.format(title, '=' * len(title))
        count = 1

        channel_names = [unicodedata.normalize('NFKD',client.get_channel(x['ChannelId']).name) for x in rank]

        longest_channel = max(channel_names, key=lambda p: len(p))

        for result in rank:
            channel = client.get_channel(result['ChannelId'])
            if channel:
                channel_name = unicodedata.normalize('NFKD', channel.name)
                num_dashes = len(longest_channel) - len(channel_name) + 2
                msg_data += '{:02}. {} {} ({} Words)\n'.format(count, unicodedata.normalize('NFKD', channel.name), '-' * num_dashes, result['NumWords'])
                count += 1

        msg_data += '```'

        if arg_obj.private:
            await client.send_message(message.author, msg_data)
        else:
            await client.send_message(message.channel, msg_data)