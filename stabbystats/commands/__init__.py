from stabbystats import RegisteredClass
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.dates as mdate
from dateutil import parser
import unicodedata
import datetime as dt
import io
import seaborn as sns

class GraphData:
    def __init__(self, title, x_label, y_label):
        self.x_data = []
        self.y_data = []
        self.title = title
        self.x_label = x_label
        self.y_label = y_label

class StabbyCommand(object, metaclass=RegisteredClass):
    """Provides a base class for commands to inherit. Contains the following class variables:
    
    command - The command typed in chat to trigger the execution of this object.
    description - The text sent from the help command.
    """
    command = None
    description = None
    usage = None
    examples = None
    admin_only = False
        
    def plot(self, data):
        plot_mult = len(data) * 100
        row = 1
        col = 1
        fig, axarr = plt.subplots(len(data), sharex=False, sharey=False)
        fig.set_size_inches(8.5, 11)
        sns.set_style("darkgrid")

        for plot_data in data:
            combined_data = zip(plot_data.x_data, plot_data.y_data)
            sorted_data = sorted(combined_data)

            new_xs = [point[0] for point in sorted_data]
            new_ys = [point[1] for point in sorted_data]

            file = io.BytesIO()

            # print(new_xs)
            # print(new_ys)

            axarr[row - 1].plot([mdate.epoch2num(d) for d in new_xs], new_ys, color = '#FF6600')
            date_fmt = '%d-%m-%y %H:%M:%S'
            date_formatter = mdate.DateFormatter(date_fmt)
            axarr[row - 1].xaxis.set_major_formatter(date_formatter)
            axarr[row - 1].xaxis_date()
            axarr[row - 1].set_xlabel(plot_data.x_label)
            axarr[row - 1].set_ylabel(plot_data.y_label)
            axarr[row - 1].set_title(unicodedata.normalize('NFKD', plot_data.title))
            axarr[row - 1].grid(True)
            row += 1

        fig.tight_layout()
        fig.autofmt_xdate()
        plt.savefig(file, format='png')
        data = file.getvalue()
        file.close()
        return data
    def str_to_channel(self, channel_str, server):
        # Some weird stuff happening here
        # Essentially return the first list element if the list isn't None, else return None
        channel = next(iter([m for m in server.channels if (m.name == channel_str or m.id == channel_str)]), None)
        
        return channel

    def str_to_role(self, role_str, server):
        role = next(iter([m for m in server.roles if (m.name == role_str or m.id == role_str)]), None)
        
        return role

    def get_epoch_seconds(self, date_str):
        pattern = '%Y-%m-%d %H:%M:%S'
        epoch = int(dt.datetime.strptime(date_str,'%Y-%m-%d %H:%M:%S').timestamp())
        return epoch

    def str_to_member(self, user_str, server):
        user_str = user_str.strip()
        
        if user_str.startswith('<@') and user_str[-1] == '>':
            user_str = user_str[2:len(user_str) - 1]

        print(user_str)
        member = server.get_member_named(user_str)

        if member == None:
            member = server.get_member(user_str)

        return member

    async def handle(self, message, args):
        raise NotImplementedError

    @classmethod
    def find(cls, pattern):
        for command in cls.registry:
            if command.command == pattern:
                return command


# Load subclasses and register them
from stabbystats.commands.help import *
from stabbystats.commands.channel import *
from stabbystats.commands.rank import *
from stabbystats.commands.leaderboard import *
from stabbystats.commands.user import *
from stabbystats.commands.info import *
from stabbystats.commands.uptime import *
from stabbystats.commands.users import *
from stabbystats.commands.enableperms import *
from stabbystats.commands.disableperms import *
from stabbystats.commands.adduser import *
from stabbystats.commands.rmuser import *
from stabbystats.commands.addrole import *
from stabbystats.commands.rmrole import *
from stabbystats.commands.permdump import *
from stabbystats.commands.overall import *