import discord 
import sqlite3
from stabbystats.commands import StabbyCommand, GraphData
from stabbystats.stabby_database.messages import Messages
from stabbystats.stabby_database.util import get_date_lookback
from stabbystats.stabby_database.util import back_unit_to_readable
import time
from datetime import timedelta
from random import randint
__all__ = ["UptimeCommand"]


class UptimeCommand(StabbyCommand):
    command = "uptime"
    description = "Get bot uptime"

    def __init__(self):
        self.start_time = time.time()

    async def handle(self, client, message, args):
        td = timedelta(seconds=time.time() - self.start_time)
        msg_data = '```Uptime: {} days {} hours {} minutes {} seconds {} ms {} us {} ns {} ps {} fs {} as```'.format(td.days, 
            td.seconds // 3600, (td.seconds % 3600) // 60, td.seconds % 60, randint(0, 999), randint(0, 999), randint(0, 999), randint(0, 999), randint(0, 999), randint(0, 999))

        await client.send_message(message.channel, msg_data)