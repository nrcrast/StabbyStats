import discord 
import sqlite3
from stabbystats.commands import StabbyCommand, GraphData
from stabbystats.stabby_database.permissions import Permissions
from stabbystats.stabby_database.util import get_date_lookback
from stabbystats.stabby_database.util import back_unit_to_readable
import unicodedata
__all__ = ["EnablePermsCommand"]


class EnablePermsCommand(StabbyCommand):
    admin_only = True
    command = "enableperms"
    description = "Enable permissions for server"
    usage = "!stabbystats enableperms"

    async def handle(self, client, message, args):
        perms = Permissions()
        perms.enable_perms(message.server.id)
        await client.send_message(message.channel, 'Enabled permissions!')