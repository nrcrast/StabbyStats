import discord 
import sqlite3
from stabbystats.commands import StabbyCommand, GraphData
from stabbystats.stabby_database.messages import Messages
from stabbystats.stabby_database.util import get_date_lookback
from stabbystats.stabby_database.util import back_unit_to_readable
import argparse
import shlex
__all__ = ["LeaderboardCommand"]


class LeaderboardCommand(StabbyCommand):
    command = "leaderboard"
    description = "Get leaderboard"

    def __init__(self):
        self.parser = argparse.ArgumentParser(prog='!stabbystats leaderboard', add_help=False, description=self.description, usage=None)
        self.parser.add_argument('--time', '-t', nargs='?', help='Time period(daily, weekly, monthly, yearly, forever)', default='daily')
        self.parser.add_argument('--count','-c', action='store_true', help='Print leaderboard in terms of msg count')
        self.parser.add_argument('--private','-p', action='store_true')
        self.usage = self.parser.format_help()

    def parse_args(self, args):
        split_args = shlex.split(args)
        try:
            arg_obj = self.parser.parse_args(split_args)

        except SystemExit:
            return None
        arg_obj.time = get_date_lookback(arg_obj.time)

        return arg_obj

    async def handle(self, client, message, args):
        msgs = Messages()

        arg_obj = self.parse_args(args)

        if not arg_obj:
            await client.send_message(message.channel, 'Sorry, I couldn\'t understand your arguments.\n```' + self.usage + '```')
            return

        leaderboard = msgs.get_leaderboard(message.server.id, arg_obj.time, message.server, count = arg_obj.count)

        title = 'Leaderboard({})'.format(back_unit_to_readable(arg_obj.time))

        msg_data = '```markdown\n{}\n{}\n\n'.format(title,'=' * len(title))
        count = 1

        longest_name = max([x['UserName'] for x in leaderboard], key=lambda p: len(p))

        unit = 'Words'

        if arg_obj.count:
            unit = 'Messages'

        for result in leaderboard:
            num_dashes = len(longest_name) - len(result['UserName']) + 2
            if result['UserName'] == message.author.name:
                msg_data += '# {:02}. {} {} ({} {})\n'.format(count, result['UserName'], '-' * num_dashes, unit, result['NumWords'])
            else:
                msg_data += '  {:02}. {} {} ({} {})\n'.format(count, result['UserName'], '-' * num_dashes, unit, result['NumWords'])
            count += 1

        msg_data += '```'

        if arg_obj.private:
            await client.send_message(message.author, msg_data)
        else:
            await client.send_message(message.channel, msg_data)