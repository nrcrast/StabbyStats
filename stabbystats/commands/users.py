import discord 
import sqlite3
from stabbystats.commands import StabbyCommand, GraphData
from stabbystats.stabby_database.members import Members
from stabbystats.stabby_database.util import get_date_lookback
from stabbystats.stabby_database.util import back_unit_to_readable
import io
import unicodedata
import argparse
import shlex

__all__ = ["UsersCommand"]


class UsersCommand(StabbyCommand):
    command = "users"
    description = "Get stats on server members"

    def __init__(self):
        self.parser = argparse.ArgumentParser(prog='!stabbystats users', add_help=False, description=self.description, usage=None)
        self.parser.add_argument('--time', '-t', nargs='?', help='Time period(daily, weekly, monthly, yearly, forever)', default='monthly')
        self.parser.add_argument('--private','-p', action='store_true')
        self.usage = self.parser.format_help()

    def parse_args(self, args):
        split_args = shlex.split(args)
        try:
            arg_obj = self.parser.parse_args(split_args)

        except SystemExit:
            return None
        arg_obj.time = get_date_lookback(arg_obj.time)

        return arg_obj

    async def handle(self, client, message, args):
        members = Members()

        arg_obj = self.parse_args(args)

        if not arg_obj:
            await client.send_message(message.channel, 'Sorry, I couldn\'t understand your arguments.\n```' + self.usage + '```')
            return
            
        members_time = GraphData('Users Online for Server {}({})'.format(unicodedata.normalize('NFKD', message.server.name), back_unit_to_readable(arg_obj.time)), 
            'Time', 'Users Online')

        data = members.get_online_users(message.server.id, arg_obj.time)
        members_time.x_data = data[0]
        members_time.y_data = data[1]

        members_overall = GraphData("Total Members for Server {}({})".format(unicodedata.normalize('NFKD', message.server.name), back_unit_to_readable(arg_obj.time)), 
            'Time', 'Members')
        data = members.get_total_users(message.server.id, arg_obj.time)
        members_overall.x_data = data[0]
        members_overall.y_data = data[1]

        if arg_obj.private:
            await client.send_file(message.author, fp = io.BytesIO(self.plot([members_overall, members_time])), filename='graph.png')
        else:
            await client.send_file(message.channel, fp = io.BytesIO(self.plot([members_overall, members_time])), filename='graph.png')