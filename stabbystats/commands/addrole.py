import discord 
import sqlite3
from stabbystats.commands import StabbyCommand, GraphData
from stabbystats.stabby_database.permissions import Permissions
from stabbystats.stabby_database.util import get_date_lookback
from stabbystats.stabby_database.util import back_unit_to_readable
import unicodedata
import argparse
import shlex
__all__ = ["AddRoleCommand"]


class AddRoleCommand(StabbyCommand):
    admin_only = True
    command = "addrole"
    description = "Add role to permissions"

    def __init__(self):
        self.parser = argparse.ArgumentParser(prog='!stabbystats addrole', add_help=False, description=self.description, usage=None)
        self.parser.add_argument('--role','-r', nargs='?', help='Role Name or ID', required = True)
        self.usage = self.parser.format_help()

    def parse_args(self, args, message):
        split_args = shlex.split(args)

        try:
            arg_obj = self.parser.parse_args(split_args)
        except SystemExit:
            return None

        if arg_obj.role:
            arg_obj.role = self.str_to_role(arg_obj.role, message.server)

        return arg_obj

    async def handle(self, client, message, args):
        perms = Permissions()
        
        arg_obj = self.parse_args(args, message)

        if not arg_obj:
            await client.send_message(message.channel, 'Sorry, I couldn\'t understand your arguments.\n```' + self.usage + '```')
            return

        if arg_obj.role == None:
            await client.send_message(message.channel, 'Sorry, cannot find role: {}'.format(args))
        else:
            perms.add_role(message.server.id, arg_obj.role.id)
            await client.send_message(message.channel, 'Added Role: {}'.format(arg_obj.role.name))