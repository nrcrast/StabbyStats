import discord
from stabbystats.commands import StabbyCommand
from stabbystats.stabby_database.permissions import Permissions

__all__ = ["HelpCommand"]


class HelpCommand(StabbyCommand):
    command = "help"
    description = "Send this help message."

    async def handle(self, client, message, args):
        print("Got help command!")
        # First add high level usage info

        em = discord.Embed(title="StabbyStats Help", colour=0x00DD00)


        print_admin = Permissions.is_member_elevated(message.author)

        for command in StabbyCommand.registry:
            cmd_help = '```markdown\n'
            if (not command.admin_only) or (command.admin_only and print_admin):
                if command.usage:
                    cmd_help += command.usage + '\n'
                else:
                    cmd_help += 'usage: !stabbystats ' + command.command + '\n\n'
                    cmd_help += command.description + '\n'
            cmd_help += '```'
            em.add_field(name=command.command, value=cmd_help, inline=False)

        await client.send_message(message.author, embed=em)
        await client.send_message(message.channel, '<@{}>, Help documentation has been sent to you over DM.'.format(message.author.id))