import discord
from stabbystats.commands import StabbyCommand
import subprocess
from stabbystats.stabby_database.messages import Messages
from stabbystats.stabby_database.members import Members


__all__ = ["OverallCommand"]


class OverallCommand(StabbyCommand):
    command = "overall"
    description = "Get overall server statistics"

    async def handle(self, client, message, args):
        msgs = Messages()
        members = Members()
        num_users = len(message.server.members)
        num_online_users = len([i for i in message.server.members if i.status != discord.Status.offline])
        max_online_users = members.get_max_online_users(message.server.id)
        unique_contributors = msgs.get_num_unique_users(message.server.id)

        msg_content = '''```markdown
# Number of Users
{num_users}

# Number of Online Users
{num_online_users}

# Max Online Users
{max_online_users} {max_online_users_date}

# Unique Contributors Today
{unique_contributors}
```'''.format(num_users = num_users, 
    num_online_users = num_online_users, 
    max_online_users = max_online_users['Num'],
    max_online_users_date = max_online_users['Date'], 
    unique_contributors = unique_contributors)

        await client.send_message(message.channel, msg_content)


        