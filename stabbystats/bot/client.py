import asyncio
import discord
import random
import string
import sqlite3
import threading
import requests
import json
from stabbystats.commands import StabbyCommand
from stabbystats.stabby_database.permissions import Permissions
import unicodedata

class StabbyStatsClient(discord.Client):
    settings = None

    def __init__(self, settings, tokens):
        self._tokens = tokens
        self._settings = settings
        self.prefix = settings.get("preferred_cmd", "")
        self.db = sqlite3.connect('stats.db')
        self.init_db()
        self.permissions_manager = Permissions()
        for command_obj in StabbyCommand.registry:
            print("Registered command module: {}".format(type(command_obj).__name__))
        super(StabbyStatsClient, self).__init__()

    def init_db(self):
        self.db.execute('CREATE TABLE IF NOT EXISTS Messages(' +
        'ID              INTEGER PRIMARY  KEY AUTOINCREMENT NOT NULL,' +
        'UserId            TEXT  NOT NULL,' +
        'Date            TIMESTAMP   default (datetime(\'now\')),' +
        'ChannelId   TEXT   NOT NULL,' +
        'Words   INTEGER   NOT NULL,' +
        'GuildId        TEXT NOT NULL);');

        self.db.execute('CREATE TABLE IF NOT EXISTS Members(' +
        'ID              INTEGER PRIMARY  KEY AUTOINCREMENT NOT NULL,' +
        'Date            TIMESTAMP   default (datetime(\'now\')),' +
        'MembersOnline   INTEGER NOT NULL,' +
        'Count           INTEGER   NOT NULL,' +
        'GuildId         TEXT NOT NULL);');

        self.db.execute('CREATE TABLE IF NOT EXISTS PermissionsEnabled(' +
        'ID              INTEGER PRIMARY  KEY AUTOINCREMENT NOT NULL,' +
        'GuildId         TEXT NOT NULL);');

        self.db.execute('CREATE TABLE IF NOT EXISTS AllowedUsers(' +
        'ID              INTEGER PRIMARY  KEY AUTOINCREMENT NOT NULL,' +
        'UserId            TEXT NOT NULL,' +
        'GuildId         TEXT NOT NULL);');

        self.db.execute('CREATE TABLE IF NOT EXISTS AllowedRoles(' +
        'ID              INTEGER PRIMARY  KEY AUTOINCREMENT NOT NULL,' +
        'RoleId            TEXT NOT NULL,' +
        'GuildId         TEXT NOT NULL);');

        self.db.execute('CREATE TABLE IF NOT EXISTS FilteredChannels(' +
        'ID              INTEGER PRIMARY  KEY AUTOINCREMENT NOT NULL,' +
        'ChannelId       TEXT NOT NULL,' +
        'GuildId         TEXT NOT NULL);');
        self.db.commit()

    def run(self):
        super(StabbyStatsClient, self).run(self._tokens['discord'])

    def insert_into_db(self, message):
        args = [message.author.id, message.channel.id, message.server.id, len(message.content.split(" "))]
        self.db.execute('INSERT INTO Messages(UserId, ChannelId, GuildId, Words) VALUES (?,?,?,?);', args)
        self.db.commit()

    def process_users_task(self):
        threading.Timer(3600, self.process_users_task).start()
        conn = sqlite3.connect('stats.db')
        for server in self.servers:
            members_online = len([i for i in server.members if i.status != discord.Status.offline])
            args = [members_online, server.member_count, server.id]
            conn.execute('INSERT INTO Members(MembersOnline, Count, GuildId) VALUES (?,?,?)', args)
            conn.commit()

    def update_server_count(self):
        payload = {'server_count': len(self.servers)}
        url = 'https://discordbots.org/api/bots/' + self.user.id + '/stats'
        headers = {'Content-Type': 'application/json', 'Authorization': self._tokens['discord_org']}
        r = requests.post(url, headers=headers, data=json.dumps(payload))

        url = 'https://bots.discord.pw/api/bots/' + self.user.id + '/stats'
        headers = {'Content-Type': 'application/json', 'Authorization': self._tokens['discord_pw']}
        r = requests.post(url, headers=headers, data=json.dumps(payload))

    async def remove_bot_farms(self):
        leaving = []
        for server in self.servers:
            num_members = len(server.members)
            num_bots = len( list(filter(lambda x: x.bot, server.members)) )
            if server.name != 'Discord Bots' and server.name != 'Discord Bot List':
                if (num_bots > num_members) or (num_bots > 50):
                    leaving.append(server)
                    
        
        for server in leaving:
            await self.leave_server(server)

    async def on_server_join(self, server):
        self.update_server_count()
        await self.remove_bot_farms()

    async def on_server_remove(self, server):
        self.update_server_count()
        await self.remove_bot_farms()

    async def on_ready(self):
        print("Logged in as {} {}".format(self.user.name, self.user.id))
        self.process_users_task()
        self.update_server_count()
        await self.remove_bot_farms()

    # def filter_codeblocks(self, message):
    #     split_content = message.content.split("```")

    async def on_message(self, message):
        if not message.server or not message.channel or message.author.bot:
            return

        self.insert_into_db(message)

        # Validate against prefix
        if not message.content.lower().startswith(tuple(self._settings['bot_cmds'])):
            return

        if not self.permissions_manager.is_user_allowed(message.author):
            return

        # Parse out command and check against all commands
        split_str = message.content.split(" ", 1)

        if len(split_str) > 1:
            contents = split_str[1]
            command, _, args = contents.strip().partition(" ")
            handler = StabbyCommand.find(command)
            if handler:
                if (not handler.admin_only) or (handler.admin_only and Permissions.is_member_elevated(message.author)):
                    await handler.handle(self, message, args)