import datetime as dt

"""Convert daily, monthly, weekly, etc. into a token to be used in an sql query"""
def get_date_lookback(args, default = '-1 day'):
    if 'daily' in args:
        return '-1 day'
    elif 'weekly' in args:
        return '-7 days'
    elif 'monthly' in args:
        return '-1 month'
    elif 'yearly' in args:
        return '-12 months'
    elif 'forever' in args:
        return None

    return default

def back_unit_to_readable(back_unit):
    if back_unit == None:
        return 'forever'

    unit_list = {'-1 day':'daily', '-7 days': 'weekly', '-1 month': 'monthly', '-12 months': 'yearly'}
    return unit_list[back_unit]


def get_epoch_seconds(date_str):
    pattern = '%Y-%m-%d %H:%M:%S'
    epoch = int(dt.datetime.strptime(date_str,'%Y-%m-%d %H:%M:%S').timestamp())
    return epoch