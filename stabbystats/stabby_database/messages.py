import sqlite3
from stabbystats.stabby_database.util import *

class RawCompiledData():

	def __init__(self, granularity_hours = 1):
		self.granularity_hours = granularity_hours
		self.x = []
		self.y = []
		self.last_t = 0
		self.current_t = 0

class Messages():
	def __init__(self):
		self.db = sqlite3.connect('stats.db')
		self.db.row_factory = sqlite3.Row

	def get_back_query(self, back_unit):
		return ' AND Date BETWEEN datetime(\'now\',\'' + back_unit + '\') AND datetime(\'now\')'

	def get_num_unique_users(self, guild_id, back_unit = '-1 day'):
		cur = self.db.cursor()
		query = 'SELECT Count(DISTINCT UserId) as UniqueUsers from Messages WHERE GuildId = ?'
		query_args=[guild_id]

		if back_unit:
			query += self.get_back_query(back_unit)

		print('Query: {} {}'.format(query, query_args))
		cur.execute(query, query_args)

		row = cur.fetchone()

		if row:
			return row['UniqueUsers']

		return None

	def get_channel_rows(self, guild_id, channel_id, back_unit):
		cur = self.db.cursor()

		if channel_id == 'all':
			query = 'SELECT * FROM Messages WHERE GuildId=?'
			query_args = [guild_id]
		else:
			query = 'SELECT * FROM Messages WHERE GuildId=? AND ChannelId=?'
			query_args = [guild_id, channel_id]

		if back_unit:
			query += self.get_back_query(back_unit)

		query += " ORDER BY Date ASC"

		print("Query: {}".format(query))
		print(query_args)

		cur.execute(query, query_args)
		return cur

	def get_rank(self, guild_id, back_unit, limit = 25):
		cur = self.db.cursor()

		query = 'SELECT ChannelId, Sum(Words) as NumWords FROM Messages WHERE GuildId = ?'

		if back_unit:
			query += self.get_back_query(back_unit)

		query += ' GROUP BY ChannelId ORDER BY Sum(Words) DESC'

		query_args = [guild_id]

		if limit:
			query += ' LIMIT ?'
			query_args.append(limit)

		results = []

		for row in cur.execute(query, query_args):
			results.append({'ChannelId': row['ChannelId'], 'NumWords': row['NumWords']})

		return results

	def get_user_data(self, guild_id, member_id, back_unit, granularity_hours = 1):
		cur = self.db.cursor()

		query = 'SELECT Words, Date FROM Messages WHERE GuildId = ? AND UserId = ?'

		if back_unit:
			query += self.get_back_query(back_unit)

		query += " ORDER BY Date ASC"

		query_args = [guild_id, member_id]

		results = []

		cur.execute(query, query_args)
		
		x_data = []
		y_data = []
		last_t_since_epoch = 0
		total_msgs = 0
		total_words = 0
		max_words = None
		min_words = None

		while True:
			row = cur.fetchone()

			if row:
				total_msgs += 1

				if max_words == None or row['Words'] > max_words:
					max_words = row['Words']

				if min_words == None or row['Words'] < min_words:
					min_words = row['Words']

				t_since_epoch = get_epoch_seconds(row['Date']) // (granularity_hours * 3600);

				# If this is within the same hour
				# Add to this data point
				if t_since_epoch == last_t_since_epoch:
					y_data[-1] += row['Words']
					total_words += row['Words']

				# New Data point
				else:
					# If there is a gap between the last time and the current time
					# Insert the appropriate amount of 0's
					if len(x_data) > 0:
						num_empties = t_since_epoch - last_t_since_epoch
						for i in range(1, num_empties):
							x_data.append((last_t_since_epoch + i) * (granularity_hours * 3600))
							y_data.append(0)

					# Start a new data point
					x_data.append(t_since_epoch * (granularity_hours * 3600))
					y_data.append(row['Words'])
					total_words += row['Words']
					last_t_since_epoch = t_since_epoch
			else:
				break

		return {'TotalMsgs': total_msgs, 'TotalWords': total_words, 'x_data': x_data, 'y_data': y_data, 'Max': max_words, 'Min': min_words}


	def get_leaderboard(self, guild_id, back_unit, server, limit = 25, count = False):
		cur = self.db.cursor()

		query_type = 'Sum(Words)'

		if count:
			query_type = 'Count(*)'

		query = 'SELECT UserId, {} as NumWords FROM Messages WHERE GuildId = ?'.format(query_type)

		if back_unit:
			query += self.get_back_query(back_unit)

		query += ' GROUP BY UserId ORDER BY {} DESC LIMIT ?;'.format(query_type)
		query_args = [guild_id, limit * 4]

		results = []

		for row in cur.execute(query, query_args):
			member = server.get_member(row['UserId'])
			if member:
				results.append({'UserName': member.name, 'NumWords': row['NumWords']})

			if len(results) == limit:
				break

		return results

	def get_msgs_channel_over_time(self, guild_id, channel_id, back_unit, granularity_hours = 1):
		print('Back unit: {}'.format(back_unit))
		cur = self.get_channel_rows(guild_id, channel_id, back_unit)
		data = {'hour': RawCompiledData(1), 'day': RawCompiledData(24), 'week': RawCompiledData(168)}
		total_msgs = 0
		total_words = 0

		while True:
			row = cur.fetchone()

			if row:
				total_msgs += 1
				total_words += row['Words']
				for time_period in data:
					#print('Time Period: {}{}'.format(total_msgs, time_period))
					data_container = data[time_period]
					data_container.current_t = get_epoch_seconds(row['Date']) // (data_container.granularity_hours * 3600);
					if data_container.current_t == data_container.last_t:
						data_container.y[-1] += 1

					else:
						# If there is a gap between the last time and the current time
						# Insert the appropriate amount of 0's
						if len(data_container.x) > 0:
							num_empties = data_container.current_t - data_container.last_t
							for i in range(1, num_empties):
								data_container.x.append((data_container.last_t + i) * (data_container.granularity_hours * 3600))
								data_container.y.append(0)

						# Start a new data point
						data_container.x.append(data_container.current_t * (data_container.granularity_hours * 3600))
						data_container.y.append(1)
						data_container.last_t = data_container.current_t
			else:
				break

		for d in data:
			print('Range: {}'.format(d))
			data_c = data[d]
			print(data_c.granularity_hours)
			print(len(data_c.x))
			print(len(data_c.y))
			#print(data_c.x)
			#print(data_c.y)

		return {'data': data, 'TotalMsgs': total_msgs, 'TotalWords': total_words}
