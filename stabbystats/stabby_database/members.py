import sqlite3
from stabbystats.stabby_database.util import *

class Members():
	def __init__(self):
		self.db = sqlite3.connect('stats.db')
		self.db.row_factory = sqlite3.Row

	def get_back_query(self, back_unit):
		return ' AND Date BETWEEN datetime(\'now\',\'' + back_unit + '\') AND datetime(\'now\')'

	def get_max_online_users(self, guild_id, back_unit = None):
		cur = self.db.cursor()

		query = 'SELECT Max(MembersOnline) as MaxMembers from Members WHERE GuildId=?'
		query_args = [guild_id]

		if back_unit:
			query += self.get_back_query(back_unit)

		cur.execute(query, query_args)

		row = cur.fetchone()

		results = {}

		if row:
			results['Num'] = row['MaxMembers']

			query = 'SELECT Date FROM Members WHERE GuildId = ? AND MembersOnline = ?'

			if back_unit:
				query += self.get_back_query(back_unit)

			query += ' ORDER BY Date DESC LIMIT 1'

			query_args.append(results['Num'])

			cur.execute(query, query_args)

			row = cur.fetchone()

			if row:
				results['Date'] = row['Date']

			return results

		return None

	def get_online_users(self, guild_id, back_unit):
		cur = self.db.cursor()


		query = 'SELECT Date, MembersOnline FROM Members WHERE GuildId=?'
		query_args = [guild_id]

		if back_unit:
			query += self.get_back_query(back_unit)

		x_data=[]
		y_data=[]

		for row in cur.execute(query, query_args):
			y_data.append(row['MembersOnline'])
			x_data.append(get_epoch_seconds(row['Date']))

		return (x_data, y_data)

	def get_total_users(self, guild_id, back_unit):
		cur = self.db.cursor()


		query = 'SELECT Date, Count FROM Members WHERE GuildId=?'
		query_args = [guild_id]

		if back_unit:
			query += self.get_back_query(back_unit)

		x_data=[]
		y_data=[]

		for row in cur.execute(query, query_args):
			y_data.append(row['Count'])
			x_data.append(get_epoch_seconds(row['Date']))

		return (x_data, y_data)

