import sqlite3
from stabbystats.stabby_database.util import *

class Permissions():
	def __init__(self):
		self.db = sqlite3.connect('stats.db')
		self.db.row_factory = sqlite3.Row

	def enable_perms(self, guild_id):
		self.disable_perms(guild_id)
		self.db.execute('INSERT INTO PermissionsEnabled(GuildId) VALUES (?)', [guild_id])
		self.db.commit()

	def disable_perms(self, guild_id):
		self.db.execute("DELETE FROM PermissionsEnabled WHERE GuildId = ?", [guild_id])
		self.db.commit()

	def add_user(self, guild_id, user_id):
		self.rm_user(guild_id, user_id)
		self.db.execute("INSERT INTO AllowedUsers(UserId,GuildId) VALUES (?,?)", [user_id, guild_id])
		self.db.commit()

	def rm_user(self, guild_id, user_id):
		self.db.execute("DELETE FROM AllowedUsers WHERE UserId = ? AND GuildId = ?", [user_id, guild_id])
		self.db.commit()

	def add_role(self, guild_id, role_id):
		self.rm_role(guild_id, role_id)
		self.db.execute("INSERT INTO AllowedRoles(GuildId,RoleId) VALUES (?,?)", [guild_id, role_id])
		self.db.commit()

	def rm_role(self, guild_id, role_id):
		self.db.execute("DELETE FROM AllowedRoles WHERE GuildId = ? AND RoleId = ?", [guild_id, role_id])
		self.db.commit()

	def get_role(self, guild, role_id):
		for role in guild.roles:
			if role.id == role_id:
				return role

		return None

	def get_roles(self, guild):
		role_names = []

		cur = self.db.cursor()
		for row in cur.execute("SELECT * FROM AllowedRoles WHERE GuildId = ?", [guild.id]):
			role = self.get_role(guild, row['RoleId'])
			if role:
				role_names.append(role.name)

		return role_names

	def get_users(self, guild):
		user_names = []

		cur = self.db.cursor()
		for row in cur.execute("SELECT * FROM AllowedUsers WHERE GuildId = ?", [guild.id]):
			member = guild.get_member(row['UserId'])
			if member:
				user_names.append(member.name)

		return user_names


	def permissions_enabled(self, guild_id):
		cur = self.db.cursor()

		cur.execute("SELECT * FROM PermissionsEnabled WHERE GuildId = ?", [guild_id])

		return (cur.fetchone() != None)

	def dumps(self, guild):
		content = '```'
		content += '** Permissions Enabled: {} **\n\n'.format(self.permissions_enabled(guild.id))
		content += '** Allowed Users **\n'
		for user in self.get_users(guild):
			content += user + '\n'

		content += '\n** Allowed Roles **\n'

		for role in self.get_roles(guild):
			content += role + '\n'

		content += '\n```'
		return content

	@staticmethod
	def is_member_elevated(member):
		return member.id == "259746215061749761" or member.server_permissions.ban_members or member.server_permissions.administrator or member.server_permissions.manage_server

	def is_user_allowed(self, member):
		if self.is_member_elevated(member):
			return True
		if not self.permissions_enabled(member.server.id):
			return True
		else:
			cur = self.db.cursor()

			for role in member.roles:
				cur.execute("SELECT * FROM AllowedRoles WHERE GuildId = ? AND RoleId = ?", [member.server.id, role.id])
				if cur.fetchone() != None:
					return True

			cur.execute("SELECT * FROM AllowedUsers WHERE GuildId = ? AND UserId = ?", [member.server.id, member.id])

			if cur.fetchone() != None:
				return True

		return False


