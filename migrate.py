import sqlite3
import discord
import asyncio
import unicodedata
import json
from datetime import datetime, date, time

db = sqlite3.connect('stats.db')

client = discord.Client()

ignore_list=["287275717639536640", "291974466966257664", "238727524803280897", "310522711245979649", "244861377984987136", "280877980002025472",
"268113241891274763", "284302743525392384", "238727785244262400", "238726053831507969", "268538021861785600", "238728664231968769", "238689533112090624",
"291996118819274755", "308772291863642112", "238828805274206210"]


db.execute('CREATE TABLE IF NOT EXISTS Messages(' +
'ID              INTEGER PRIMARY  KEY AUTOINCREMENT NOT NULL,' +
'UserId            TEXT  NOT NULL,' +
'Date            TIMESTAMP   default (datetime(\'now\')),' +
'ChannelId   TEXT   NOT NULL,' +
'Words   INTEGER   NOT NULL,' +
'GuildId        TEXT NOT NULL);');

def get_oldest_message(channel_id):
	cur = db.cursor()
	cur.execute('SELECT Date from Messages WHERE ChannelID = ? ORDER BY Date ASC LIMIT 1', [channel_id])
	row = cur.fetchone()

	if row:
		return datetime.strptime(row[0], "%Y-%m-%d %H:%M:%S")
	else:
		return datetime.now()

def insert_into_db(message):
	time = message.timestamp.strftime("%Y-%m-%d %H:%M:%S")
	args = [time, message.author.id, message.channel.id, message.server.id, len(message.content.split(" "))]
	db.execute('INSERT INTO Messages(Date, UserId, ChannelId, GuildId, Words) VALUES (?,?,?,?,?);', args)

@client.event
async def on_ready():
	print('Logged in as')
	print(client.user.name)
	print(client.user.id)
	print('------')
	#238666723824238602
	server = client.get_server("238666723824238602")
	print(unicodedata.normalize('NFKD', server.name))
	channel_list = [channel for channel in server.channels if channel.type == discord.ChannelType.text and channel.id not in ignore_list]
	grabbed_logs = False

	while True:
		grabbed_logs = False
		for channel in channel_list:
			oldest = get_oldest_message(channel.id)
			time_delta = datetime.now() - oldest

			if time_delta.days <= 365:
				count = 0
				async for message in client.logs_from(channel, limit=500, before=oldest):
					if not message.author.bot:
						grabbed_logs = True
						count += 1
						insert_into_db(message)
				db.commit()
				if count > 0:
					print("Inserted {} into {} (delta {} days)".format(count, unicodedata.normalize('NFKD', channel.name), time_delta.days))
		if not grabbed_logs:
			break
	print("Finished!")

with open('tokens.json', 'r') as f:
	tokens = json.load(f)
	client.run(tokens['discord'])