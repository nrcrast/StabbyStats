import json
import sys
from stabbystats.bot.client import StabbyStatsClient


def main():
    with open('settings.json', 'r') as settings_file:
    	with open('tokens.json', 'r') as tokens_file:    
        	settings = json.load(settings_file)
        	tokens = json.load(tokens_file)
    client = StabbyStatsClient(settings, tokens)
    client.run()

if __name__ == "__main__":
    main()