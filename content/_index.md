## Overview

StabbyStats is a Discord bot designed to give server administrators useful statistics about their members. 

Features:

* Rank channels by messages per hour (over past day, week, month, year, all time)
* Graph of any channel activity (over past day, week, month, year, all time)
* Graph of your own activity (over past day, week, month, year, all time)
* Graph of online users (over past day, week, month, year, all time)
* User Leaderboard (over past day, week, month, year, all time)
* Fully configurable by server administrators (or anyone with ban priviledge)

## Usage

*Note:* You can use either `!stats`, `!stabbystats` or `!stabbybot` as a prefix


### overall
```
usage: !stabbystats overall

Get overall server statistics
```
### user
```
usage: !stabbystats user [--user [USER]] [--time [TIME]] [--private]

Get stats on a single user(or yourself). If private modifier is used, data is
sent via DM.

optional arguments:
  --user [USER], -u [USER]
                        Username or User ID
  --time [TIME], -t [TIME]
                        Time period(daily, weekly, monthly, yearly, forever)
  --private, -p
```
### leaderboard
```
usage: !stabbystats leaderboard [--time [TIME]] [--private]

Get leaderboard

optional arguments:
  --time [TIME], -t [TIME]
                        Time period(daily, weekly, monthly, yearly, forever)
  --private, -p
```
### channel
```
usage: !stabbystats channel [--channel [CHANNEL]] [--time [TIME]] [--private]

Get stats on a specific channel

optional arguments:
  --channel [CHANNEL], -c [CHANNEL]
                        Channel Name or ID
  --time [TIME], -t [TIME]
                        Time period(daily, weekly, monthly, yearly, forever)
  --private, -p
```
### users
```
usage: !stabbystats users [--time [TIME]] [--private]

Get stats on server members

optional arguments:
  --time [TIME], -t [TIME]
                        Time period(daily, weekly, monthly, yearly, forever)
  --private, -p
```
### rank
```
usage: !stabbystats rank [--time [TIME]] [--private] [--all]

Rank channels

optional arguments:
  --time [TIME], -t [TIME]
                        Time period(daily, weekly, monthly, yearly, forever)
  --private, -p
  --all, -a
```
### uptime
```
usage: !stabbystats uptime

Get bot uptime
```
### info
```
usage: !stabbystats info

Get bot info.
```
### adduser
```
usage: !stabbystats adduser [--user [USER]]

Add user to permissions

optional arguments:
  --user [USER], -u [USER]
                        Username or User ID
```
### rmuser
```
usage: !stabbystats rmuser [--user [USER]]

Remove user from permissions

optional arguments:
  --user [USER], -u [USER]
                        Username or User ID
```
### addrole
```
usage: !stabbystats addrole [--role [ROLE]]

Add role to permissions

optional arguments:
  --role [ROLE], -r [ROLE]
                        Role Name or ID
```
### rmrole
```
usage: !stabbystats rmrole [--role [ROLE]]

Remove role from permissions

optional arguments:
  --role [ROLE], -r [ROLE]
                        Role Name or ID
```                     
### permdump
```
usage: !stabbystats permdump
```
### disableperms
```
!stabbystats disableperms
```
### enableperms
```nolang
usage: !stabbystats enableperms
```

*Note:* All commands below `!stabbystats info` are only available to server administrators

## Examples

<!-- ### Users
`!stabbystats users monthly`
![Users Graph(Monthly)](img/users_monthly.png)

`!stabbystats users daily`
![Users Graph(Daily)](img/users_daily.png)

### Channel Rank
`!stabbystats rank all`
![Rank all channels(Daily)](img/rank_all.png)

`!stabbystats rank all forever`
![Rank all channels(Forever)](img/rank_all_forever.png)

### Me
`!stabbystats me monthly`
![User Stats(Monthly)](img/me_monthly.png)

### Leaderboard
`!stabbystats leaderboard forever`
![Leaderboard(Forever)](img/leaderboard_forever.png)

## Administration Examples

*Note:* Permissions are disabled by default when StabbyStats joins your server. This means anyone can summon the bot. This does *not* mean, however, that anyone can configure the bot. Only users with the 'ban' priviledge can configure the bot.

### Enable permissions rules
`!stabbystats enableperms`

### Disable permissions rules
`!stabbystats disableperms`

### Dump permissions data
`!stabbystats dumpperms`
![Dump Permissions](img/dump_perms.png)

### Add Role
Allow users with specified role to summon bot

`!stabbystats addrole Helpers`

### Remove Role
`!stabbystats rmrole Helpers`

### Add User
Allow specific user to summon bot (username can be ID or display name)

`!stabbystats adduser SuperStabby`

### Remove User
`!stabbystats rmuser SuperStabby`

### Add Channel Filter
Filter channels you don't care about from rank command.

`!stabbystats addchanfilter welcome-channel`

### Remove Channel Filter
`!stabbystats rmchanfilter welcome-channel` -->