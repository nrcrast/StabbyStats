"""Setup script for elmerbot.
"""
from setuptools import setup
from codecs import open
from os import path

VERSION = "1.0.0"
DESCRIPTION = "Statistics monitoring bot for Discord"

here = path.abspath(path.dirname(__file__))

with open(path.join(here, "README.md"), encoding="utf-8") as filein:
    long_description = filein.read()

with open(path.join(here, "requirements.txt"), encoding="utf-8") as filein:
    requirements = [line.strip() for line in filein.readlines()]

setup(
    name="stabbystats",
    version=VERSION,
    description=DESCRIPTION,
    long_description=long_description,
    author="superstabby",
    classifiers=["Development Status :: 3 - Alpha",
                 "Intended Audience :: Developers",
                 "Programming Language :: Python :: 3"],
    packages=["stabbystats", "stabbystats.bot", "stabbystats.commands"],
    install_requires=requirements
)